db.users.insertOne({
	name: "Single",
	accommodates: '2',
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available : 10,
	isAvaialble: false
});

db.users.insertMany([
	{
		name: "Double",
		accommodates: '3',
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available : 5,
		isAvaialble: false
	},
	{
		name: "Queen",
		accommodates: '4',
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple gateway",
		rooms_available : 15,
		isAvaialble: false
	}
]);

// finding
db.users.find({name: "Double"});

// updateOne
db.users.updateOne(
	{rooms_available : 15},
	{
		$set: {rooms_available : 0}
	}
);

// deleting
db.users.deleteMany({
	rooms_available : 0
});

// can also write 
// db.getCollection.insertOne()
// db.getCollection.updateOne()
// db.getCollection.deleteOne()
// db.getCollection.deleteMany()........