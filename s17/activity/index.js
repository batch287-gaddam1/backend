/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function firstFunction(){
		let fullName = prompt("Enter your Full Name");
		let age = prompt("Enter your age");
		let location = prompt("Enter your address");

		console.log("Hello " + fullName);
		console.log("You are "  + age + " years old.");
		console.log("You live in "  + location);

		alert("Thank you for the details!");
	};

	firstFunction();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favMusicians(){
		let musician1 = "DSP";
		let musician2 = "Thaman";
		let musician3 = "Anup Rubens";
		let musician4 = "Chakri";
		let musician5 = "MM Keeravani";

		console.log("My favorite Musicians:");
		console.log("1. " + musician1);
		console.log("2. " + musician2);
		console.log("3. " + musician3);
		console.log("4. " + musician4);
		console.log("5. " + musician5);
	};

	favMusicians();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favMovies(){
		let movie1 = "Baahubali";
		let movie2 = "Saaho";
		let movie3 = "Chitralahari";
		let movie4 = "Nannaku Prematho";
		let movie5 = "Venky";

		console.log("My favorite Movies are:");
		console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating: 87%");
		console.log("2. " + movie2);
		console.log("IMDB Rating: 5");
		console.log("3. " + movie3);
		console.log("IMDB Rating: 7.1");
		console.log("4. " + movie4);
		console.log("IMDB Rating: 7.5");
		console.log("5. " + movie5);
		console.log("IMDB Rating: 7.4");
	};

	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();// will give error as it was defined inside function.
// printFriends();// will give error as it should be called after function.
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);//Gives error as these are defined inside 
// i.e., these are locally defined not globally defined.
// console.log(friend2);//gives error.
