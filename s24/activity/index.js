const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}.`);



const address = ["India",'Telangana','Hyderabad',505450];
// variable address as an array

const [country,state,city, pincode]  = address;

console.log(`I live at ${city}, ${state}, ${country}, ${pincode}.`);

let animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: 1075,
	measurements: "20 ft 3 in"
};//variable animal with object data type

let {name,type,weight,measurements} = animal;
console.log(`${name} was a ${type}. He weighted at ${weight} kgs with a measurement of ${measurements}.`);

const numbers = [2,4,6,8,10,12,14,16];
numbers.forEach((number) => console.log(`${number}`));

let reduceNumber = numbers.reduce((x,y) => x + y);//reduce array method
console.log(`Sum of all the numbers is: ${reduceNumber}`);

class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}//Dog class

let myDog = new Dog('Frankie',5,'Miniature Dachshund');
console.log(myDog);