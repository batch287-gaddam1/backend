console.log('Hello');

let trainer = {
		name: 'Ash Ketchum',
		age: 10,
		pokemon: ['Pikachu','Charizard','Squirtle','Bulbasaur'],
		friends: {
			hoenn: ['May','Max'],
			kanto: ['Brock','Misty']
		},
		talk: function(){
			console.log('Pikachu I choose you!');
			
		}
		
	};

	console.log(trainer);
	console.log('Result of dot notation:');
	console.log(trainer.name);
	console.log('Result of square bracket notation:');
	console.log(trainer['pokemon']);
	trainer.talk();

function Pokemon(name,level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			if(target.health > 0){
				targetPokemonHealth = target.health-this.attack;
				target.health = targetPokemonHealth;
				console.log(this.name + " tackled "+ target.name);
				
				console.log(target.name+"'s health is now reduced to "+ targetPokemonHealth);

			} else {
				target.faint();
			};
			// or (but above one gives correct compared to below one)
			/*
			targetPokemonHealth = target.health-this.attack;
			target.health = targetPokemonHealth;
			console.log(this.name + " tackled "+ target.name);
			
			console.log(target.name+"'s health is now reduced to "+ targetPokemonHealth);
			if(target.health <= 0){
				target.faint();
			};
			*/

		};
		this.faint = function(){
			console.log(this.name + ' has fainted ');
		};
	};

	let pikachu = new Pokemon("Pikachu",12);
	let geodude = new Pokemon('Geodude',8);
	let mewtwo = new Pokemon('Mewtwo',100);

	console.log(pikachu);
	console.log(geodude);
	console.log(mewtwo);

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewtwo.tackle(geodude);
	geodude.faint();
	console.log(geodude);

	mewtwo.tackle(geodude);