const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {

	if (request.url == '/login'){

		response.writeHead(200, {'constant-Type': 'text/plain'})
		response.end('You are in the Login page!')
	} else if (request.url == '/register'){

		response.writeHead(200, {'constant-Type': 'text/plain'})
		response.end('Im sorry the page you are looking cannot be found!')
	} else {
		
		response.writeHead(404, {'constant-Type': 'text/plain'})
		response.end('Page not available!')	
	}
});

server.listen(port);

console.log(`Server is now accesible at localhost:${port}.`);