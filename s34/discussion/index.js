// Use the require directive to load the express module/package
// It allows us to access to methods and functions that will allow us to easily create a server
const express = require("express");

// Creates an application using express
// In layman's term, app is our server
const app = express();//triggering our variable express and load the data in app

// For our application server to run, we need a port to listen to 
const port = 3001;//any number you want to write

// Use middleware to allow express to read JSON
app.use(express.json()) // It means we want to load express in json format

// Use middleware to allows express to able to read more data types from a response
app.use(express.urlencoded({extended:true}));//entended accepts boolean values : true/false

// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send(`Hellow from the /hello endpoint!`);
})

app.post("/display-name", (req,res) => {
	console.log(req.body);
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// Sign-up in our Users

let users = []; // This is our mock database

app.post("/signup", (req,res) => {

	console.log(req.body);
	// If contents of the "request body" with the property "username" and "password" is not empty
	if (req.body.username !== "" && req.body.password !== ""){
		
		// This will store the user object sent via Potman to the users array created above
		users.push(req.body);

		// Recognize the success of the request
		res.send(`User ${req.body.username} successfully registered!`);
		console.log(users);
	} else {
		res.send(`Please input both username and password`)
	}

});

// This route expects to receive a PUT request at the URI "/change-password"

app.put("/change-password", (req,res) => {

	// Creates a variable to store the message to be sent back to nthe client/Postman
	let message;

	console.log(users.length);
	// Creates a for loop that will loop through the elements of the "users" array
	for( let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the username of the current object in the loop is same, run the following code
		if(req.body.username == users[i].username){

			// Changes the users[i].password value to the req.body.password
			users[i].password = req.body.password;

			// Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`;

			console.log("Newly updated mock database");
			console.log(users);

			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;//to end the cycle

		// If no user is found
		} else {

			// Changes the message to be sent by the response
			message = `User does not exist.`
		}
	}

	// Send a response back to the client/Postman once the password has been updated or if a user is not found
	res.send(message);
})

app.listen(port, () => console.log(`Server is currently running at port ${port}`));