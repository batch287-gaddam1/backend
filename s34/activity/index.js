const express = require("express");
const app = express();
const port = 3002;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (req,res) => {
	res.send(`Welcome to the home page`)
})

let users = [];

app.post("/add-user", (req,res) => {
	if (req.username !== "" && req.password !== ""){
		users.push(req.body);
		res.send("User added")
	} else{
		res.send(`Please enter Username and Password!`)
	}
})

app.get("/users", (req,res) => {
	res.send(users)
	console.log(users)
})

app.delete("/delete-user", (req,res) => {
	for( let i = 0; i < users.length; i++ ){
		if (req.body.username === users[i].username){
			res.send(`User ${req.body.username} has been deleted!`);
			// break
		} else {
			res.send(`User not Found!!!!`)
		}
	}
})

app.listen(port, () => console.log(`Server is now running at localhost: ${port}.`))