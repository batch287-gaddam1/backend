console.log("Hello");

// [ SECTION ] Functions

	// [ SUB-SECTION ] Parameters and Arguements

	// Functions in JS are lines/blocks of codes that tell our device/application/browser to perform a certain task when called/invoked/triggered.

	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi, "+ nickname);

	};

	// printInput();

	// For other cases, functions can also process data directly passed into it
	//  instead of relying only on Global Variable and prompt().

	// name112312121212 only ACTS like a VARIABLE

		function printName(name112312121212){ //name112312121212 is the parameter

			// This one work as well:
			// let name = name112312121212;
			// console.log("Hi, " + name);
			console.log("Hi, " + name112312121212);
			// console.log("Hi, " + name112312121212);

		};
		// console.log(name112312121212); // This gives error as it is not defined.

		printName("Juana"); //"Juana" is arguement.

		// You can directly pass data into the function.
		//  The function can then call/use that data which is referred
		//  as "name112312121212" within the function.

		printName("Rana");

		// When the "printName()" function is called again, it 
		// stores the value of "Rana" in the parameter "name112312121212" 
		// then uses it to print a message.

		/*Variables can also passed as an arguement.*/
		let sampleVariable = "Yui";

		printName(sampleVariable);

		/*Function arguements cannot be used by a function
		 if there are no parameters provided within the function.*/

		printName();//It will be Hi, undefined

		function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of "+ num + " divided by 8 is: " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8("12");//better to avoid "" for numbers.
		checkDivisibilityBy8(16);
		checkDivisibilityBy8(52);

		/*
			You can also do the same using prompt(), however,
			take note that prompt() outputs a string. Strings
			are not ideal for mathematical computations.
		*/

	// Functions as Arguements

		// Function parameters can also accept other function as arguements

		function arguementFunction(){
			console.log("This function was passed as an arguement before the message was printed.");

		};

		// function invokeFunction(arguementFunction){
		// 	arguementFunction();

		// };

		function invokeFunction(iAmNotRelated){
			iAmNotRelated();
			// console.log(iAmNotRelated);
			arguementFunction();

		};

		invokeFunction(arguementFunction);

		// Adding and removing the paranthesis "()" impacts the 
		//  output of JS heavily.

		console.log(arguementFunction);
		// will provide information about a function in the console using console.log();

	// Using Multiple parameters

		// Multiple "aaarguements" will correspond to the number of "parameters"
		//  declared in a function in succeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
			console.log(middleName + " " + lastName + " " + firstName);


		};

		createFullName("Mike", "Kel", "Jordan");
		createFullName("Mike", "Jordan");
		createFullName("Mike", "Kel", "Jordan", "Mike");

		/*
		In JS, providing more/less arguements than expected parameters
		 will not return an error.
		*/

		// Using Variable as Arguements

		let firstName = "Dwayne";
		let secondName = "The Rock";
		let lastName = "Johnson";

		createFullName(firstName, secondName, lastName);

// [ SECTION ] The Return Statement

	// let promptVariable = prompt("avava");
	// The "return" statement allows us to output a value
	// from a function to be passed to the line/block of
	// code that invoked/called the function.

		function returnFullName(firstName, middleName, lastName){
			console.log("This message is from console.log");
			return firstName + " " + middleName + ' ' +lastName;
			console.log("This message is from console.log");
		};

		let completename = returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completename);

		// In our example, the 'returnFullName' function eas invoked
		// called in the same line as declaring a variable.

		// Whatever value is returned from the "returnFullName" function 
		// is stored in the "completename" variable.

		// Notice that the console.log() after the return is no longer
		// printed in the console that is because ideally any line/block
		// of code that comes after the return statement is ignored 
		// because it ends the function execution.

		console.log(returnFullName(firstName, secondName, lastName));
		// In this example, console.log() will print the returned 
		// value of the returnFullName() function.

		// You can also create a variable inside the function to 
		// contain the result and return that variable instead.

		function returnAddress(city, country){
			let fullAddress = city + " , " + country;
			return fullAddress;
		};

		let myAddress = returnAddress("Manila City", "Philippines");
		console.log(myAddress);

		// On the other hand, when a function the only has console.log()
		// to display its result will be undefined instead.

		function printPlayerInfo(username, level, job){
			console.log("Username: " + username);
			console.log("Level: " + level);
			console.log("Job: " + job);
		};

		let user1 = printPlayerInfo("knight_white", 95, 'Paladin');
		console.log(user1);

		// Returns undefined because printPlayerInfo return nothing.It
		// only console.log the details.

		// You cannot save any value from printPlayerInfo() 
		// because it does not return anything.