const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");
// Setup the server
const app = express();
const port = 5002;
app.use(express.json());
app.use(express.urlencoded( { extended: true } ));
// Setup the Database

	// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.3q4pfjm.mongodb.net/s36-activity",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	mongoose.connection.once("open", () => console.log("we're connected to the cloud database!"));

	app.listen(port, () => console.log(`Currently listening to port ${port}`));

app.use("/tasks", taskRoute);