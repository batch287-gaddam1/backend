// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Setup the server
const app = express();
const port = 5001;
app.use(express.json());
app.use(express.urlencoded( { extended: true } ));

// Setup the Database

	// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.3q4pfjm.mongodb.net/s36-discussion",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	mongoose.connection.once("open", () => console.log("we're connected to the cloud database!"));

	// Server listening //it can be at any place , not mandatort to be at the end
	app.listen(port, () => console.log(`Currently listening to port ${port}`));

// Add the tasks route
// Allow us to all the task routes created in "taskRoute.js" file to use "/tasks" route
// http://localhost:5001/tasks
app.use("/tasks", taskRoute);