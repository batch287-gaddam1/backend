// Find users with letter 's' in their firstName or 'd'
// in their lastName
db.users.find(
	{$or: [
	  { firstName: { $regex: 's', $options: 'i'} }, 
	  { lastName: { $regex: 'd', $options: 'i'} } 
	  ]
	},
	{
		_id : 0
	}
);

// Find users who are from the HR department and their age is 
//  greater than or equal to 70
db.users.find(
	{
		$and: [ {department: "HR"}, {age: {$gte: 70} } ]
	}
);

// Find users with letter 'e' in their firstName and has an 
// age of less than or equal to 30
db.users.find(
	{
		$and: [ 
			{ firstName: { $regex: 'e', $options: 'i'} },
			{ age: {$lte: 30} }
		]
	}
);