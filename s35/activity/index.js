const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3004;
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.3q4pfjm.mongodb.net/s35-activity", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("we're connected to the cloud database."));
// User Schema
const activityTaskSchema = new mongoose.Schema({
	username: String,
	password: String
	
});
// User Model
const Task = mongoose.model("Task", activityTaskSchema);
// Creating todo list routes
app.use(express.json());
app.use(express.urlencoded( { extended: true } ));
app.post("/signup", (req,res) => {
	Task.findOne({username: req.body.username}).then((result,err) => {
		if(result != null && result.username == req.body.username){
			return res.send("User already exists!");
		} else {
			let newActivityTask = new Task({
				username: req.body.username,
				password: req.body.password
			});
			newActivityTask.save().then((savedTask,saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New User Registered");
				}
			})
		}
	})
})

app.get("/signup", (req,res) => {
	Task.find({}).then((result,err) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at ${port}`));