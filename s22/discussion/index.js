console.log("Hello World!");

// Array Methods

	// JS has built-in functions and methods for arrays. This allows
	// us to manipulate and access array items.

	// Mutators Method

		// Mutators methods are functions that "mutate" or change
		// an array after theu're created

	let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];

	// Push()
		// Adds an element in the end of an array AND returns the 
		// array's length
		// Syntax:
			// arrayName.push();

	console.log("Current array:");
	console.log(fruits);
	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength);
	console.log('Mutated array from push method:');
	console.log(fruits);

	fruits.push('Avocado', 'Guava');
	console.log('Mutated array from push method:');
	console.log(fruits);

	// pop()
		// Removes the last element in an array AND returns 
		// the removed element
		// Syntax:
			// arrayName.pop();

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from pop method:");
	console.log(fruits);

	// unshift()
		// Adds one or more elements at the beginning of an array
		// Syntax:
			// arrayName.unshift('elementA');
			// arrayName.unshift('elementA','elementB');

	let unshift = fruits.unshift('Lime','Banana');
	console.log(unshift);
	console.log("Mutated array from unshift method:");
	console.log(fruits);

	// shift()
		// Removes an element at the befinning of an array 
		// AND returns the removed element
		// Syntax:
			// arrayName.shift();

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method:");
	console.log(fruits);

	// splice()
		// Simultaneously removes elements from a specified 
		// index number and adds elements
		// Syntax:
			// arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);

	let splice = fruits.splice(1,2,'Lime','Cherry');
	console.log(splice);
	console.log('Mutated array from splice method:');
	console.log(fruits);

	// sort()
		// Rearranges the array elements in alphanumeric order
		// Syntax:
			// arrayName.sort();

	fruits.sort();
	console.log("Mutated array from sort method:");
	console.log(fruits);

	// reverse()
		// Reverses the order of array elements
		// Syntax:
			// arrayName.reverse();

	fruits.reverse();
	console.log('Mutated array from reverse method:');
	console.log(fruits);

	// Non-Mutators Method
	
		// Non-Mutators methods are functions that do not modify
		// or change an array after they're created

	let countries = ['US','PH','CAN','SG','TH','PH','FR','IND'];

	// indexOf()
		// Returns the index number of the first matching element
		// found in an array
		// If no match was found, the result will be -1.
		// Syntax:
			// arrayName.indexOf(searchValue);

	let firstIndex = countries.indexOf('PH');
	console.log('Result of indexOf method: '+ firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log('Result of indexOf method: '+ invalidCountry);

	// lastIndexOf()
		// Returns the index number of the last matching element
		// found in an array
		// Syntax:
			// arrayName.lastIndexOf(searchValue, fromIndex);

	let lastIndex = countries.lastIndexOf('PH');
	console.log("Result of lastIndex method: "+ lastIndex);

	// Getting the index number starting from a specified index
	let lastIndexStart = countries.lastIndexOf('PH',4);
	console.log("Result of lastIndexStart method: "+ lastIndexStart);

	// slice()
		// Portions/slices elements from an array AND returns 
		// a new array
		// Syntax:
			// arrayName.slice(startingIndex);

	console.log(countries);
	// Slicing off elements from a specified index to the last element
	let slicedArrayA = countries.slice(2);
	console.log('Result from slice method: ');
	console.log(slicedArrayA);
	console.log(countries);//no change for non-mutators

	// Slicing off elements from a specified index to another index
	let slicedArrayB = countries.slice(2,4);
	console.log('Result from slice method: ');
	console.log(slicedArrayB);
	// results ['CAN','SG']; starts fron index number 2 and ends at 
	// index number 4, index 4 is not included.
	
	// Slicing off elements starting from the last element of an array
	let slicedArrayC = countries.slice(-3);
	console.log('Result from slice method');
	console.log(slicedArrayC);

	// toString()
		// Returns an array as a string separated by commas
		// Syntax:
			// arrayName.toString();

	let stringArray = countries.toString();
	console.log('Result from toString method:');
	console.log(stringArray);

	// concat()
		// combines two arrays and returns the combined result
		// Syntax:
			// arrayA.concat(arrayB);

	let taskArrayA = ['drink html','eat javascript'];
	let taskArrayB = ['inhale css','breath sass'];
	let taskArrayC = ['get git','be bootstrap'];

	let task = taskArrayA.concat(taskArrayB);
	console.log('Result from concat method:');
	console.log(task);

	// Combining multiple arrays
	console.log('Result from concat method:');
	let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
	console.log(allTasks);

	// Combine arrays with elements
	let combinedTasks = taskArrayA.concat('smell express','throw react');
	console.log('Result from concat method:');
	console.log(combinedTasks);

	// join()
		// Returns an array as a string separated by specified
		// separator string
		// Syntax:
			// arrayName.join('seoeratorString');

	let users = ['John','Jane','Joe','Robert'];

	console.log(users.join());
	console.log(users.join(""));
	console.log(users.join(" - "));

	// Iterations Methods
		// Iterations method are loops designed to perform 
		// repetitive task on arrays

	// forEach()
		// similar to a for loop that iterates on each array element
		// forEach() does not return anything,just runs the statement
		// Syntax:
			// arrayName.forEach(function(indivElement){
			//	 statement
			// });

	allTasks.forEach(function(task){
		console.log(task)
	});

	let filteredTasks = [];

	// Looping through All Array Items
		// It's good practice to print the current element 
		// in the console when working with array iterations

	allTasks.forEach(function(task){
		console.log(task);

		if (task.length > 10){
			console.log(task);
			filteredTasks.push(task);
		};
	});

	console.log("Result of filtered task:");
	console.log(filteredTasks);

	// map()
		// Iterates on each element AND returns new array
		// with different values depending on the result of
		// the function's operation
		// Unlike the forEach method, the map method requires
		// the use of "return" statement in order to create another
		// array with the performed operation
		// Syntax:
			// let/const resultArray = arrayNAme.map(function(indivElement))

	let numbers = [1,2,3,4,5];

	let numberMap = numbers.map(function(number){
		return number * number;
	});

	console.log('Original Array:');
	console.log(numbers);
	console.log('Result of map method:');
	console.log(numberMap);

	// map() vs. forEach()

	let numberForEach = numbers.forEach(function(number){
		return number*number
	});

	console.log(numberForEach); //gives undefined

	// forEach(), loops over all items in the array as does 
	// map(), but forEach() does not return a new array.

	// every()
		// checks if all elements in an array meet the given 
		// condition
		// Returns a true value if all elements meet the condition
		// and false if otherwise
		// Syntax:
			// let/const resultArray = arrayName.every(function(indivElem){
				//return expression/condition;
			// })

	let allValid = numbers.every(function(number){
		return (number < 3)
	});

	console.log("Result of every method:");
	console.log(allValid);

	// some()
		// checks if atleast one element in the array meets the
		// condition
		// Syntax:
			// let/const resultArray = arrayName.some(function(indivElem){
				//return expression/condition;
			// })

	let someValid = numbers.some(function(number){
		return ( number < 2);
	});

	console.log("Result of some method:");
	console.log(someValid);

	if(someValid){
		console.log('Some numbers in the array are greater than 2');
	};

	// filter()
		// Returns a new array that contains elements which 
		// meets the given condition
		// Syntax:
			// let/const resultArray = arrayName.filter(function(indivElem){
				//return expression/condition;
			// });

	let filterValid = numbers.filter(function(number){
		return (number < 3);
	});

	console.log("result of filter method:");
	console.log(filterValid);

	let nothingFound = numbers.filter(function(number){
		return (number = 0);
	});

	console.log("result of filter method:");
	console.log(nothingFound);

	// Filtering using forEach

	let filteredNumbers = [];

	numbers.forEach(function(number){
		console.log(number);

		if(number < 3){
			filteredNumbers.push(number);
		};
	});

	console.log("Result of filter method:");
	console.log(filteredNumbers);

	let products = ['Mouse','Keyboard','Laptop','Monitor'];

	// includes()
		// includes() method checks if the arguement passed can
		//  be found in the array
		// Syntax:
			// arrayName.includes(<arguementToFind>);

	let productFound1 = products.includes('Mouse');
	console.log(productFound1);

	productFound1 = products.includes('MousE');
	console.log(productFound1);//it is case sensitive, gives false

	productFound1 = products.includes('Headset');
	console.log(productFound1);

	// Chaining Methods
		// The result of the first method is used on the second
		// method until all 'chained'

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	});

	console.log(filteredProducts);

	// reduce()
		// Evaluates elements from left to right and returns/reduce
		// the array into a single value
		// Syntax:
			// let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
				//return expression/operation
			// })

		// How the 'reduce' method works
			// 1. The first/result element in the array is stored
			// in the 'accumulator' parameter
			// 2. The second/next element in the array is stored
			// in the 'currentValue' parameter
			// 3. An operation is performed on the two elements
			// 4. The loop repeats steps 1-3 until all elements ....

	let iteration = 0;
	console.log(numbers);
	let reducedArray = numbers.reduce(function(x,y){

		console.warn('current iteration: ' + ++iteration);
		console.log('accumulator: ' + x);
		console.log('currentValue: ' + y);

		return x + y;
	});

	console.log('Result of reduce method: ' + reducedArray);

	// Reducing String Arrays

	let list = ['Hellow','Again','Wurld'];

	let reducedJoin = list.reduce(function(x,y){
		return x + ' ' + y;
	});

	console.log('Result of reduce method: ' + reducedJoin);