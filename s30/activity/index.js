// To count the total no.of fruits on sale
db.fruits.aggregate([
      { $match: { onSale: true} },
      { $count: "fruitsOnSale"}
]);

// Count the total no.of fruits with stock more than or equal to 20
db.fruits.aggregate([
      { $match: { onSale: true, stock: {$gte: 20} } },
      { $count: "enoughStock"}
]);

// To get the average price of fruits onSale per supplier
db.fruits.aggregate([
      { $match: { onSale: true } },
      { $group: { _id: "$supplier_id" , avg_price: {$avg: "$price" } } }
]);

// To get the highest price of a fruit per supplier
// in order of _id 2,1 (using of sort)
db.fruits.aggregate([
      { $match: { onSale: true } },
      { $group: { _id: "$supplier_id" , max_price: {$max: "$price" } } },
      { $sort: { _id: -1 } }
]);

// To get the lowest price of a fruit per supplier
// in order of _id 1,2 (using of sort)
db.fruits.aggregate([
      { $match: { onSale: true } },
      { $group: { _id: "$supplier_id" , min_price: {$min: "$price" } } },
      { $sort: { _id: 1 } }
]);